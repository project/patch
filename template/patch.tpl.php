<?php

/**
 * @file
 * patch.tpl.php
 *
 * Template to render a patch. 
 *
 * Available variables:
 * - $files: an ordered array containing the structures with information about changed files.
 *
 */
?>
<div class="diff">
	<a href='#' onclick='return(toggle());'>Toggle symbols</a>
  <?php foreach ($files as $file): ?>
	  <?php echo $file; ?>
  <?php endforeach; ?>

</div>
