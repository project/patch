<?php

/**
 * @file
 * patch-chunk.tpl.php
 *
 * Template to render a single chunk (change hunk) of a patch. One patch has one or more files,
 * and each file has one or more chunks.
 *
 * Available variables:
 * - $info
 * - $lines: an ordered array of lines for this chunk. Each line has been previously rendered 
 *           by patch-line.tpl.php.
 *
 */
?>
<tr>
  <th>old</th>
  <th>new</th>
  <th><?php echo $info['func']; ?></th>
</tr>

<?php foreach ($lines as $line): ?>

  <?php echo $line; ?>

<?php endforeach; ?>