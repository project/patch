<?php

/**
 * @file
 * patch-line.tpl.php
 *
 * Template to render a single line of a patch. 
 *
 * Available variables:
 * - $line: an associative array containing the following keys:
 *     - line_numbers: an associative array containing the old and new line number for this line.
 *     - content: the contents of the line.
 *     - classes: an array of classes for this line.     
 *
 */
?>
<tr class='<?php echo implode(' ', $line['classes']); ?>'>
  <td class='line-number old'>
    <?php if (isset($line['line_numbers']['old'])) echo $line['line_numbers']['old']; ?>
	</td>
  <td class='line-number new'>
    <?php if (isset($line['line_numbers']['new'])) echo $line['line_numbers']['new']; ?>
  </td>
  <td class='content'><?php echo $line['content']; ?></td>
</tr>