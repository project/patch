<?php

/**
 * @file
 * patch-file.tpl.php
 *
 * Template to render a single file of a patch. 
 *
 * Available variables:
 * - $info: an associative array containing the following keys:
 *     - old_name: the file name for the old file (before change).
 *     - new_name: the file name for the new file (after change).
 * - $chunks: an ordered array of chunks for this file. The chunks are rendered by patch-chunk.tpl.php.
 */
?>
<table class='file'>
  <tr>
    <td colspan='3'>
      <span class="filename">
        <?php echo $info['old_name']; ?>
        <?php if ($info['old_name'] != $info['new_name']): ?>
          &raquo; <?php echo $info['new_name']; ?>
        <?php endif; ?>
      </span>
    </td>
  </tr>

	<?php foreach ($chunks as $chunk): ?>
  	<?php echo $chunk; ?>
  <?php endforeach; ?>

</table>