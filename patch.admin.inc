<?php

/**
 * @file
 * patch.admin.inc
 */


// TODO: refine.
/**
 * Administrative settings form for the patch module.
 */
function patch_admin() {
  $form['patch_allowed_hosts'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed hosts'),
    '#default_value' => variable_get('patch_allowed_hosts', 'drupal.org'),
    '#description' => t("Patches can only be downloaded from the hosts entered here. Leave empty if you do not want to restrict patch downloading by host."),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

function patch_admin_validate($form_id, &$form_state) {
  $hosts = explode(',', $form_state['values']['patch_allowed_hosts']);
  foreach ($hosts as &$host) {
    $host = trim($host);
  }
  $form_state['values']['patch_allowed_hosts'] = implode(',', $hosts);
}