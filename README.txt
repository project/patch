
DESCRIPTION
-----------
Patch lets users view unified diff patches in a user-friendly format, including line-numbers,
color-coded changes and control characters (such as tabs, spaces, line feeds, ...)

REQUIREMENTS
------------
This version of patch is designed for Drupal 6.x. 
There are currently no dependencies on other modules.

INSTALLATION
------------
1. Untar the tarball into your module directory (usually sites/all/modules)
2. Enable the patch module via Administration > Site building > Modules (in the Development section)
3. (Optional) Give "Access Patches" permission to everyone role that should be able to view
   patches via this module.

USAGE
-----
After installing and enabling Patch, and after setting the necessary permissions, authorized users
will be able to view patches through the patch-path (www.example.com/patch) in one of two ways:

- by specifying the URL where a patch resides via the url query parameter:
  http://www.example.com/patch?url=http://drupal.org/files/issues/job_queue_2.patch

- by specifying the path to a patch-file in the URL:
  http://www.example.com/patch/subdir/example.patch
	In the example above, subdir/example.patch is located in your site's files-directory.

NOTES: 1. Patch only accepts files with .patch and .diff extensions as valid input. Only patches
          in unified diff format are supported. 
       2. When using the url query parameter, only patches from allowed hosts are downloaded. The
          list of allowed hosts can be configured via Administer > Site configuration > Patch.

THEMEING
--------
The following templates are used for themeing:
- patch-line.tpl.php: theme a single line of code in a patch.
- patch-chunk.tpl.php: theme a single chunk (change hunk) in a patch.
- patch-file.tpl.php: theme a single file in a patch.
- patch.tpl.php: theme the shell for the patch.